# Integration scenarios CoreD

This family of scenarios integrates components that enable the SLA remediation phase. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-D1
This scenario integrates the Diagnosis and the RDS components (Enforcement module), which analyse monitoring events and prepare remediation plans according to the performed analysis. 

Details:
- Base Scenario ID: /
- Added artefacts: Planning, Implementation, Diagnosis, RDS

Involved components:
- SLA Platform:	/
- Negotiation module: /
- Enforcement module: Planning, Implementation, Diagnosis, RDS
- Monitoring module: /
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /