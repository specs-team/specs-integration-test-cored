{
  "security_mechanism_id": "SM2",
  "security_mechanism_name": "sva",
  "sm_description": "This security mechanisms offers resilience to software vulnerabilities, achieved through periodic vulnerability scans and/or reports about available updates and upgrades of vulnerable libraries installed on the system.",
  "security_capabilities": [
    "software_vulnerability"
  ],
  "enforceable_metrics": [
    "basic_scan_frequency_m13",
    "list_update_frequency_m14",
    "extended_scan_frequency_m22",
    "up_report_frequency_m23",
    "pen_testing_activated_m24"
  ],
  "monitorable_metrics": [
    "basic_scan_frequency_m13",
    "list_update_frequency_m14",
    "extended_scan_frequency_m22",
    "up_report_frequency_m23",
    "pen_testing_activated_m24",
    "repository_availability_sva_msr6",
    "list_availability_sva_msr7",
    "scanners_availability_sva_msr8",
    "scan_report_availability_sva_msr9",
    "up_report_availability_sva_msr10"
  ],
  "measurements": [
    {
      "msr_id": "report_basic_age_sva_msr1",
      "msr_description": "Age of the scanning report (basic scan).",
      "frequency": "basic_scan_frequency_m13",
      "metrics": [
        "basic_scan_frequency_m13"
      ],
      "monitoring_event": {
        "event_id": "basic_report_too_old_sva_e1",
        "event_description": "Scanning report (basic scan) is too old.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "gt",
          "threshold": "metric:basic_scan_frequency_m13"
        }
      }
    },
    {
      "msr_id": "list_age_sva_msr2",
      "msr_description": "Age of the vulnerability list.",
      "frequency": "list_update_frequency_m14",
      "metrics": [
        "list_update_frequency_m14"
      ],
      "monitoring_event": {
        "event_id": "list_too_old_sva_e2",
        "event_description": "Vulnerability list is too old.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "gt",
          "threshold": "metric:list_update_frequency_m14"
        }
      }
    },
    {
      "msr_id": "report_extended_age_sva_msr3",
      "msr_description": "Age of the scanning report (extended scan).",
      "frequency": "extended_scan_frequency_m22",
      "metrics": [
        "extended_scan_frequency_m22"
      ],
      "monitoring_event": {
        "event_id": "extended_report_too_old_sva_e3",
        "event_description": "Scanning report (extended scan) is too old.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "gt",
          "threshold": "metric:extended_scan_frequency_m22"
        }
      }
    },
    {
      "msr_id": "up_report_age_sva_msr4",
      "msr_description": "Age of the update/upgrade report.",
      "frequency": "up_report_frequency_m23",
      "metrics": [
        "up_report_frequency_m23"
      ],
      "monitoring_event": {
        "event_id": "up_report_too_old_sva_e4",
        "event_description": "Update/upgrade report is too old.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "gt",
          "threshold": "metric:up_report_frequency_m23"
        }
      }
    },
    {
      "msr_id": "pen_testing_activated_sva_msr5",
      "msr_description": "Activation of the penetration testing functionality.",
      "frequency": "0",
      "metrics": [
        "pen_testing_activated_m24"
      ],
      "monitoring_event": {
        "event_id": "pen_testing_misconfigured_sva_e5",
        "event_description": "Penetration testing misconfigured.",
        "event_type": "VIOLATION",
        "condition": {
          "operator": "neq",
          "threshold": "metric:pen_testing_activated_m24"
        }
      }
    },
    {
      "msr_id": "repository_availability_sva_msr6",
      "msr_description": "Availability of the vulnerability repositories.",
      "frequency": "1h",
      "metrics": [
        "list_update_frequency_m14"
      ],
      "monitoring_event": {
        "event_id": "repository_unavailable_sva_e6",
        "event_description": "Repository for extracting published vulnerabilities is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "false"
        }
      }
    },
    {
      "msr_id": "list_availability_sva_msr7",
      "msr_description": "Availability of vulnerability list.",
      "frequency": "1h",
      "metrics": [
        "basic_scan_frequency_m13",
        "extended_scan_frequency_m22"
      ],
      "monitoring_event": {
        "event_id": "list_unavailable_sva_e7",
        "event_description": "Vulnerability list is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "false"
        }
      }
    },
    {
      "msr_id": "scanner_availability_sva_msr8",
      "msr_description": "Availability of the installed scanner.",
      "frequency": "1h",
      "metrics": [
        "basic_scan_frequency_m13",
        "extended_scan_frequency_m22",
        "pen_testing_activated_m24"
      ],
      "monitoring_event": {
        "event_id": "scanner_unavailable_sva_e8",
        "event_description": "Installed scanner is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "false"
        }
      }
    },
    {
      "msr_id": "scan_report_availability_sva_msr9",
      "msr_description": "Availability of the scanning report.",
      "frequency": "1h",
      "metrics": [
        "up_report_frequency_m23"
      ],
      "monitoring_event": {
        "event_id": "scan_report_unavailable_sva_e9",
        "event_description": "Scanning report is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "false"
        }
      }
    },
    {
      "msr_id": "up_report_availability_sva_msr10",
      "msr_description": "Availability of the update/upgrade report.",
      "frequency": "1h",
      "metrics": [
        "up_report_frequency_m23"
      ],
      "monitoring_event": {
        "event_id": "up_report_unavailable_sva_e10",
        "event_description": "Update/upgrade report is unavailable.",
        "event_type": "ALERT",
        "condition": {
          "operator": "eq",
          "threshold": "false"
        }
      }
    }
  ],
  "metadata": {
    "components": [
      {
        "component_name": "sva_dashboard",
        "component_type": "dashboard",
        "cookbook": "SVA",
        "recipe": "install_SVA_Dashboard",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": true,
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "string"
              ],
              "source_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                "80",
                "443"
              ]
            },
            "outcoming": {
              "destination_ips": [
                "string"
              ],
              "destination_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                "22"
              ]
            }
          }
        }
      },
      {
        "component_name": "sva_enforcement",
        "component_type": "enforcement",
        "cookbook": "SVA",
        "recipe": "install_SVA_Enforcement",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": true,
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "string"
              ],
              "source_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                80,
                443
              ]
            },
            "outcoming": {
              "destination_ips": [
                "string"
              ],
              "destination_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                "22"
              ]
            }
          }
        }
      },
      {
        "component_name": "sva_monitoring",
        "component_type": "monitoring",
        "cookbook": "SVA",
        "recipe": "install_SVA_Monitoring",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": true,
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "string"
              ],
              "source_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                80,
                443
              ]
            },
            "outcoming": {
              "destination_ips": [
                "string"
              ],
              "destination_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                "22"
              ]
            }
          }
        }
      },
      {
        "component_name": "sva_openscap",
        "component_type": "scanner",
        "cookbook": "SVA",
        "recipe": "install_OpenSCAP",
        "implementation_step": 1,
        "pool_seq_num": 1,
        "pool_id": "webpool",
        "vm_requirement": {
          "hardware": "t1_micro",
          "usage": "50",
          "acquire_public_ip": true,
          "private_ips_count": 1,
          "firewall": {
            "incoming": {
              "source_ips": [
                "string"
              ],
              "source_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                80,
                443
              ]
            },
            "outcoming": {
              "destination_ips": [
                "string"
              ],
              "destination_nodes": [
                "string"
              ],
              "interface": "public/private:int",
              "proto": [
                "string"
              ],
              "port_list": [
                "22"
              ]
            }
          }
        }
      }
    ],
    "constraints": [
      {
        "ctype": "SC1a",
        "arg1": [
          "sva_dashboard"
        ],
        "arg2": [
          "sva_enforcement",
          "sva_monitoring",
          "sva_openscap"
        ]
      },
      {
        "ctype": "SC2a_1",
        "arg1": [
          "sva_dashboard"
        ],
        "op": "=",
        "n1": "1"
      },
      {
        "ctype": "SC2c",
        "arg1": [
          "sva_enforcement"
        ],
        "arg2": [
          "sva_dashboard"
        ]
      },
      {
        "ctype": "SC2c",
        "arg1": [
          "sva_monitoring"
        ],
        "arg2": [
          "sva_dashboard"
        ]
      },
      {
        "ctype": "SC2c",
        "arg1": [
          "sva_openscap"
        ],
        "arg2": [
          "sva_dashboard"
        ]
      }
    ]
  },
  "remediation": {
    "remediation_actions": [
      {
        "name": "sva_a1",
        "action_description": "Check if the configured repository is available.",
        "recipes": [
          "invoke_sva-msr6"
        ]
      },
      {
        "name": "sva_a2",
        "action_description": "Reconfigure repository and check if it is available.",
        "recipes": [
          "reconfigure_repository",
          "invoke_sva-msr6"
        ]
      },
      {
        "name": "sva_a3",
        "action_description": "Check if the vulnerability list is available.",
        "recipes": [
          "invoke_sva-msr7"
        ]
      },
      {
        "name": "sva_a4",
        "action_description": "Delete old vulnerability list, generate new vulnerability list, and check if it is available.",
        "recipes": [
          "delete_old_vulnerability_list",
          "generate_vulnerability_list",
          "invoke_sva-msr7"
        ]
      },
      {
        "name": "sva_a5",
        "action_description": "Check if installed scanners are available.",
        "recipes": [
          "invoke_sva-msr8"
        ]
      },
      {
        "name": "sva_a6",
        "action_description": "Delete old scanning report, scan again, and check if the new scanning report is available.",
        "recipes": [
          "delete_old_scanning_report",
          "perform_vulnerability_scan",
          "invoke_sva-msr9"
        ]
      },
      {
        "name": "sva_a7",
        "action_description": "Reinstall scanners and check if they are available.",
        "recipes": [
          "reinstall_scanners",
          "invoke_sva-msr8"
        ]
      },
      {
        "name": "sva_a8",
        "action_description": "Check if the scanning report is available.",
        "recipes": [
          "invoke_sva-msr9"
        ]
      },
      {
        "name": "sva_a9",
        "action_description": "Delete old report, check for upgrades/updates and check if the new report is available.",
        "recipes": [
          "delete_old_up_report",
          "check_for_upgrades",
          "invoke_sva-msr10"
        ]
      }
    ],
    "remediation_flow": [
      {
        "name": "basic_report_too_old_sva_e1",
        "action_id": "sva_a3",
        "yes_action": {
          "action_id": "sva_a5",
          "yes_action": {
            "action_id": "sva_a6",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": {
            "action_id": "sva_a7",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": "notify"
          }
        },
        "no_action": {
          "action_id": "sva_a4",
          "yes_action": {
            "action_id": "sva_a5",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": {
              "action_id": "sva_a7",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": "observe",
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          },
          "no_action": {
            "action_id": "sva_a1",
            "yes_action": "notify",
            "no_action": {
              "action_id": "sva_a2",
              "yes_action": {
                "action_id": "sva_a4",
                "yes_action": {
                  "action_id": "sva_a6",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          }
        }
      },
      {
        "name": "list_too_old_sva_e2",
        "action_id": "sva_a1",
        "yes_action": {
          "action_id": "sva_a4",
          "yes_action": "observe",
          "no_action": "notify"
        },
        "no_action": {
          "action_id": "sva_a2",
          "yes_action": {
            "action_id": "sva_a4",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "extended_report_too_old_sva_e3",
        "action_id": "sva_a3",
        "yes_action": {
          "action_id": "sva_a5",
          "yes_action": {
            "action_id": "sva_a6",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": {
            "action_id": "sva_a7",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": "notify"
          }
        },
        "no_action": {
          "action_id": "sva_a4",
          "yes_action": {
            "action_id": "sva_a5",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": {
              "action_id": "sva_a7",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": "observe",
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          },
          "no_action": {
            "action_id": "sva_a1",
            "yes_action": "notify",
            "no_action": {
              "action_id": "sva_a2",
              "yes_action": {
                "action_id": "sva_a4",
                "yes_action": {
                  "action_id": "sva_a6",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          }
        }
      },
      {
        "name": "up_report_too_old_sva_e4",
        "action_id": "sva_a8",
        "yes_action": {
          "action_id": "sva_a9",
          "yes_action": "observe",
          "no_action": "notify"
        },
        "no_action": {
          "action_id": "sva_a3",
          "yes_action": {
            "action_id": "sva_a5",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": {
                "action_id": "sva_a9",
                "yes_action": "observe",
                "no_action": "notify"
              },
              "no_action": "notify"
            },
            "no_action": {
              "action_id": "sva_a7",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": {
                  "action_id": "sva_a9",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          },
          "no_action": {
            "action_id": "sva_a4",
            "yes_action": {
              "action_id": "sva_a5",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": {
                  "action_id": "sva_a11",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": {
                "action_id": "sva_a7",
                "yes_action": {
                  "action_id": "sva_a6",
                  "yes_action": {
                    "action_id": "sva_a9",
                    "yes_action": "observe",
                    "no_action": "notify"
                  },
                  "no_action": "notify"
                },
                "no_action": "notify"
              }
            },
            "no_action": {
              "action_id": "sva_a1",
              "yes_action": "notify",
              "no_action": {
                "action_id": "sva_a2",
                "yes_action": {
                  "action_id": "sva_a4",
                  "yes_action": {
                    "action_id": "sva_a6",
                    "yes_action": {
                      "action_id": "sva_a9",
                      "yes_action": "observe",
                      "no_action": "notify"
                    },
                    "no_action": "notify"
                  },
                  "no_action": "notify"
                },
                "no_action": "notify"
              }
            }
          }
        }
      },
      {
        "name": "pen_testing_misconfigured_sva_e5",
        "action_id": "sva_a7",
        "yes_action": "observe",
        "no_action": "notify"
      },
      {
        "name": "repository_unavailable_sva_e6",
        "action_id": "sva_a2",
        "yes_action": "observe",
        "no_action": "notify"
      },
      {
        "name": "list_unavailable_sva_e7",
        "action_id": "sva_a1",
        "yes_action": {
          "action_id": "sva_a4",
          "yes_action": "observe",
          "no_action": "notify"
        },
        "no_action": {
          "action_id": "sva_a2",
          "yes_action": {
            "action_id": "sva_a4",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": "notify"
        }
      },
      {
        "name": "scanner_unavailable_sva_e8",
        "action_id": "sva_a7",
        "yes_action": "observe",
        "no_action": "notify"
      },
      {
        "name": "scan_report_unavailable_sva_e9",
        "action_id": "sva_a3",
        "yes_action": {
          "action_id": "sva_a5",
          "yes_action": {
            "action_id": "sva_a6",
            "yes_action": "observe",
            "no_action": "notify"
          },
          "no_action": {
            "action_id": "sva_a7",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": "notify"
          }
        },
        "no_action": {
          "action_id": "sva_a4",
          "yes_action": {
            "action_id": "sva_a5",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": "observe",
              "no_action": "notify"
            },
            "no_action": {
              "action_id": "sva_a7",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": "observe",
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          },
          "no_action": {
            "action_id": "sva_a1",
            "yes_action": "notify",
            "no_action": {
              "action_id": "sva_a2",
              "yes_action": {
                "action_id": "sva_a4",
                "yes_action": {
                  "action_id": "sva_a6",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          }
        }
      },
      {
        "name": "up_report_unavailable_sva_e10",
        "action_id": "sva_a8",
        "yes_action": {
          "action_id": "sva_a9",
          "yes_action": "observe",
          "no_action": "notify"
        },
        "no_action": {
          "action_id": "sva_a3",
          "yes_action": {
            "action_id": "sva_a5",
            "yes_action": {
              "action_id": "sva_a6",
              "yes_action": {
                "action_id": "sva_a9",
                "yes_action": "observe",
                "no_action": "notify"
              },
              "no_action": "notify"
            },
            "no_action": {
              "action_id": "sva_a7",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": {
                  "action_id": "sva_a9",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": "notify"
            }
          },
          "no_action": {
            "action_id": "sva_a4",
            "yes_action": {
              "action_id": "sva_a5",
              "yes_action": {
                "action_id": "sva_a6",
                "yes_action": {
                  "action_id": "sva_a11",
                  "yes_action": "observe",
                  "no_action": "notify"
                },
                "no_action": "notify"
              },
              "no_action": {
                "action_id": "sva_a7",
                "yes_action": {
                  "action_id": "sva_a6",
                  "yes_action": {
                    "action_id": "sva_a9",
                    "yes_action": "observe",
                    "no_action": "notify"
                  },
                  "no_action": "notify"
                },
                "no_action": "notify"
              }
            },
            "no_action": {
              "action_id": "sva_a1",
              "yes_action": "notify",
              "no_action": {
                "action_id": "sva_a2",
                "yes_action": {
                  "action_id": "sva_a4",
                  "yes_action": {
                    "action_id": "sva_a6",
                    "yes_action": {
                      "action_id": "sva_a9",
                      "yes_action": "observe",
                      "no_action": "notify"
                    },
                    "no_action": "notify"
                  },
                  "no_action": "notify"
                },
                "no_action": "notify"
              }
            }
          }
        }
      }
    ]
  },
  "chef_recipes": [
    {
      "name": "install_OpenSCAP",
      "recipe_description": "Install OpenSCAP.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
      ]
    },
    {
      "name": "install_Nikto",
      "recipe_description": "Install Nikto.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
      ]
    },
    {
      "name": "install_OpenVAS",
      "recipe_description": "Install OpenVAS.",
      "associated_metrics": [],
      "associated_measurements": [],
      "dependent_components": [
      ]
    },
    {
      "name": "install_SVA_Enforcement",
      "recipe_description": "Install SVA Enforcement.",
      "associated_metrics": [
        "basic_scan_frequency_m13",
        "list_update_frequency_m14",
        "extended_scan_frequency_m22",
        "up_report_frequency_m23"
      ],
      "associated_measurements": [],
      "dependent_components": [
        "sva_monitoring",
        "sva_dashboard",
        "sva_openscap",
        "sva_openvas",
        "sva_nikto"
      ]
    },
    {
      "name": "install_SVA_Monitoring",
      "recipe_description": "Install SVA Monitoring.",
      "associated_metrics": [
        "basic_scan_frequency_m13",
        "list_update_frequency_m14",
        "extended_scan_frequency_m22",
        "up_report_frequency_m23"
      ],
      "associated_measurements": [
        "report_basic_age_sva_msr1",
        "list_age_sva_msr2",
        "report_extended_age_sva_msr3",
        "up_report_age_sva_msr4",
        "repository_availability_sva_msr6",
        "list_availability_sva_msr7",
        "scanner_availability_sva_msr8",
        "scan_report_availability_sva_msr9",
        "up_report_availability_sva_msr10"
      ],
      "dependent_components": [
        "sva_enforcement",
        "sva_dashboard",
        "sva_openscap",
        "sva_openvas",
        "sva_nikto"
      ]
    },
    {
      "name": "install_SVA_Dashboard",
      "recipe_description": "Install SVA Dashboard.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "invoke_sva-msr6",
      "recipe_description": "Invoke SVA Monitoring to take measurement repository_availability_sva_msr6 and label the event as remediation_event.",
      "associated_metrics": [],
      "associated_measurements": [
        "repository_availability_sva_msr6"
      ],
      "dependent_components": [
        "sva_monitoring"
      ]
    },
    {
      "name": "reconfigure_repository",
      "recipe_description": "Reconfigure repository.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "invoke_sva-msr7",
      "recipe_description": "Invoke SVA Monitoring to take measurement list_availability_sva_msr7 and label the event as remediation_event.",
      "associated_metrics": [],
      "associated_measurements": [
        "list_availability_sva_msr7"
      ],
      "dependent_components": [
        "sva_monitoring"
      ]
    },
    {
      "name": "delete_old_vulnerability_list",
      "recipe_description": "Delete old vulnerability list.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "generate_vulnerability_list",
      "recipe_description": "Generate vulneability list.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "invoke_sva-msr8",
      "recipe_description": "Invoke SVA Monitoring to take measurement scanner_availability_sva_msr8 and label the event as remediation_event.",
      "associated_metrics": [],
      "associated_measurements": [
        "scanner_availability_sva_msr8"
      ],
      "dependent_components": [
        "sva_monitoring"
      ]
    },
    {
      "name": "delete_old_scanning_report",
      "recipe_description": "Delete old scanning report.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "perform_vulnerability_scan",
      "recipe_description": "Perform vulnerability scan.",
      "associated_metrics": [
        "basic_scan_frequency_m13",
        "extended_scan_frequency_m22"
      ],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "invoke_sva-msr9",
      "recipe_description": "Invoke SVA Monitoring to take measurement scan_report_availability_sva_msr9 and label the event as remediation_event.",
      "associated_metrics": [],
      "associated_measurements": [
        "scan_report_availability_sva_msr9"
      ],
      "dependent_components": [
        "sva_monitoring"
      ]
    },
    {
      "name": "reinstall_scanners",
      "recipe_description": "Reinstall scanners.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_openscap",
        "sva_openvas",
        "sva_nikto"
      ]
    },
    {
      "name": "delete_old_up_report",
      "recipe_description": "Delete old up report.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "check_for_upgrades",
      "recipe_description": "Check for updates/upgrades.",
      "associated_metrics": [],
      "associated_measurements": [
      ],
      "dependent_components": [
        "sva_enforcement"
      ]
    },
    {
      "name": "invoke_sva-msr10",
      "recipe_description": "Invoke SVA Monitoring to take measurement up_report_availability_sva_msr10 and label the event as remediation_event.",
      "associated_metrics": [],
      "associated_measurements": [
        "up_report_availability_sva_msr10"
      ],
      "dependent_components": [
        "sva_monitoring"
      ]
    }
  ]
}
